text_filament_family="SPECIAL"; // Must be capital letters
text_filament_name="nGen_LUX";
text_filament_brand="colorFabb";
text_filament_color="Regal Violet";

text_filament_id="#O2";
text_printer="I3 MK3SMMU2S";
text_print_settings="Slic3rPE 0.20mm";

//Set font size in mm
textsize_upper=4; 
textsize_lower=5; 
textsize_steps=5;
textsize_back=4;

// Help -> Font List
font_normal="DejaVu Sans:style=Normal";
font_bold="DejaVu Sans:style=Bold";

//Advanced test swatch patterns active? If you want a simple swatch, set this to disabled
test_patterns="enabled";//[enabled,disabled]

//Number of advanced test circles?
test_circles=5;//[0:5]

// Thickness progression of the 'layers' is taken from https://www.thingiverse.com/thing:3067940 : 0.2, 0.4, 0.6, 0.8, 1.0, 1.6, without the 2.25 mm. If you put a negative number here, it will generate a "bridge" structure of the respective thickness.
thickness_steps=[-.6, 0.2, 0.4, 0.6, 0.8, 1.0, 1.6]; 

//textheight on thickness steps
textheight_on_steps=.4;

//Swatch size. This is compatible to Jaxels box. Other sizes are e.g. 75,40,3. 
//Swatch Width
w=79.5; 
//Swatch Height
h=30; 
//Swatch Thickness
th=2.2;
 
//Swatch roundness radius
round=5;

//Border size around the text
wall=2;

//Text line separation
linesep=1.25;

//hole radius. Set to 0 to have no hole
r_hole=2.2;

//side idents to take out the swatch from the box. set to 0 to have no idents
r_ident=2;
 

/* [Hidden] */
//-------------------------------------------------------------------------------

/*
Configurable Filament Swatch 
Version N, March 2019
Written by MC Geisler (mcgenki at gmail dot com)

This is a swatch with configurable text.

Have fun!

License: Attribution 4.0 International (CC BY 4.0)

You are free to:
    Share - copy and redistribute the material in any medium or format
    Adapt - remix, transform, and build upon the material
    for any purpose, even commercially.
*/


$fn=48;

font_recessed=0.8;

module rounded_square(x,y,z,r)
{
    delta=r;
    
    hull()
    {
        translate([r,r,0]) cylinder(h=z,r=r);
        translate([-r+x,r,0]) cylinder(h=z,r=r);
        translate([r,-r+y,0]) cylinder(h=z,r=r);
        translate([-r+x,-r+y,0])cylinder(h=z,r=r);
    }
}

module rounded_square_test_pattern(x,y,z,r)
{
    delta=r;
    r3=z/2;
    r2=r;

    difference()
    {
        hull()
        {
            //bottom left cylindrical/round
            translate([r,r,0]) cylinder(h=z,r=r);
            
            //bottom right test flattened/round
            translate([-r+x,r,0]) cylinder(h=z,r=r);
            
            //upper left ball/rounded
            /// translate([r,-r+y,0]) cylinder(h=z,r=r);
            translate([r3,-r3+y,r3]) scale([r3,r3,z/2]) sphere(r=1);
            
            //upper right corner
            r_corner=.1;
            translate([-r_corner+x,-r_corner+y,0]) cylinder(h=z,r=r_corner);
        }
        
        for (i = [0:test_circles])
        {
            //circle top left with round test pattern
            translate([wall+r_hole+r_hole*2.5*i,h-wall-r_hole,th/2-.1])
                cylinder(r=r_hole, h=th+.2, center=true);
        }          
        
        //cuts
        cutin=wall*1.2;
        cutwide=1;
        wallwide_from=.5;
        wallwide_step=.1;
        wallwide_number=5;
        for(i=[0:wallwide_number-1])
        {
            wallwide=wallwide_from+wallwide_step*i;
            translate([x-r-r2-(cutwide+wallwide)*i,-.1,-.1]) 
                cube([cutwide,cutin+.1,z+.2]);
        }
        
        //overhang
        i=wallwide_number-1;
        short_x=(x-r-r2-(cutwide+wallwide_from+wallwide_step*i)*i);
        translate([0,-.01,-z])
            rotate([0,-asin(2*z/short_x),0])
                cube([short_x,wall*1.2,z]);
        
        //text on top
        fontdepth=.6;
        translate([x-wall,y-fontdepth,z-.2]) 
            rotate([-90,0,0])
                linear_extrude(height=fontdepth*2,convexity = 10)
                    text(text_filament_family,size=z-.5,font=font_bold,halign="right",valign="bottom", spacing=1.5);        
    }
}


module rounded_square_test_pattern_raised(x,y,z,r,test_patterns)
{ 
    //sphere test
    translate([wall+r_hole+r_hole*2.5,h-wall-r_hole,th/2])
        intersection()
        {
            cylinder(r=r_hole, h=th, center=true);
            translate([0,0,-r_hole+th/2])
                sphere(r=r_hole);
        }
        
    //cylinder/hole test
    r_testhole=.5;
    r_testcylinder=r_testhole+.5;
    translate([wall+r_hole+r_hole*5,h-wall-r_hole,th/2])
        difference()
        {
            union()
            {
                cylinder(r=r_testcylinder, h=th, center=true);
                translate([-r_testhole,-r_hole,-th/2])
                    cube([r_testhole*2,r_hole*2,.2],center=false);
                translate([-r_hole,-r_testhole,th/2-.2])
                    cube([r_hole*2,r_testhole*2,.2],center=false);
            }
            cylinder(r=r_testhole, h=th+.2, center=true);
        }
    
    //overhang test
    bridge_h=.2;
    bridge_pos=th/2;
    translate([wall+r_hole+r_hole*7.5,h-wall-r_hole,bridge_pos])
            translate([-r_hole/2,-r_hole,0])
                cube([r_hole,r_hole*2,bridge_h],center=false);
        
    //hanging cylinder test
    testradius=(z-.3)/2;
    translate([wall+r_hole+r_hole*10, h-wall-r_hole*.5, th-testradius])
                rotate([90,0,0])
                    cylinder(r=testradius,h=r_hole*1.7,center=false);
 
     //pyramid test
    testpyramid=z-.3;
    biggerpyramid=1;
    translate([wall+r_hole+r_hole*12.5, h-wall-r_hole*2, th-z/2])
        rotate([-90,45,0])
            cylinder(d1=z*sqrt(2), d2=0, h=testpyramid, $fn=4);
    translate([wall+r_hole+r_hole*12.5, h-wall, th-z/2])
        intersection()
        {
            rotate([90,0,0])
                cylinder(d1=z*sqrt(2), d2=0, h=testpyramid*1, $fn=4);
            cube([3*z,3*z,z],center=true);
        }
}

module textlines()
{
    translate([ w-wall,h-wall-textsize_upper,th-font_recessed])
        linear_extrude(height=th,convexity = 10)
            text(text_filament_brand,size=textsize_upper,font=font_normal,halign="right",valign="baseline");  
    
    translate([ wall,h-wall-(textsize_upper+textsize_lower*linesep),th-font_recessed])
        linear_extrude(height=th,convexity = 10)
                text(text_filament_name,size=4,font=font_normal,halign="left",valign="baseline");  
    
    translate([ w-wall,h-wall-(textsize_upper+textsize_lower*linesep),th-font_recessed])
        linear_extrude(height=th,convexity = 10)
            text(text_filament_color,size=textsize_lower,font=font_normal,halign="right",valign="baseline");
    
    translate([ w-wall,h-wall-textsize_back,font_recessed])
        rotate([0, 180, 0]) 
            linear_extrude(height=th,convexity = 10)
                text(text_printer,size=textsize_back,font=font_normal,halign="left",valign="baseline");
    
    translate([ w-wall,h-wall-(textsize_back+textsize_back*linesep),font_recessed ])
        rotate([0, 180, 0]) 
            linear_extrude(height=th,convexity = 10)
                text(text_print_settings,size=textsize_back ,font=font_normal,halign="left",valign="baseline");
                
    translate([ wall,h-wall-(textsize_back+textsize_back*linesep),font_recessed ])
        rotate([0, 180, 0]) 
            linear_extrude(height=th,convexity = 10)
                text(text_filament_id,size=textsize_back ,font=font_normal,halign="right",valign="baseline");
}


module swatch(test_patterns)
{            
    //wall thickness around step area 
    stepareadistance=(test_patterns=="enabled"?1.5:1);
    //height of the text field -> needs more space if raised text
    font_h= 1 + textsize_upper + linesep*textsize_lower;
    //step area height
    steparea_h=h-(stepareadistance+1)*wall-font_h-wall;
    //if raised text, what is the area to recess so the raised text goes on top
    fontarea_h=font_h+wall;
        
    difference()
    {
        if(test_patterns=="enabled")
            rounded_square_test_pattern(w,h,th,round);
        else
            rounded_square(w,h,th,round);

        //hole to hang
        translate([wall+r_hole,h-wall-r_hole,th/2])
            cylinder(r=r_hole, h=th*2, center=true);
                            
        for (i = [0:len(thickness_steps)-1])
        {
            steparea_w=(w-(2*stepareadistance)*wall)/len(thickness_steps)*(i+1);
            
            translate([ wall*stepareadistance,wall*stepareadistance,(thickness_steps[i]<0?-.1:thickness_steps[i]) ])
            {
                rounded_square(steparea_w,steparea_h,th*2,round-wall);
            }
        }
        
        //handles
        handle_groove_out=r_ident/2;
        translate([-handle_groove_out,h-wall-r_ident,th/2])
            cylinder(r=r_ident, h=th*2, center=true);
        translate([w+handle_groove_out,h-wall-r_ident,th/2])
            cylinder(r=r_ident, h=th*2, center=true);
        
        //text
        textlines();
    }

    //thickness numbers & bridges
    textheight=textheight_on_steps;
    textheight_bridge=textheight_on_steps;
    
    one_steparea_w=(w-2*stepareadistance*wall)/len(thickness_steps);
    for (i = [0:len(thickness_steps)-1])
    {
        steparea_w=one_steparea_w*(i+1);
        translate([ wall*stepareadistance+ steparea_w,0,0 ])
        {    
            {
                rotate([0, 0, 90])
                    translate([ one_steparea_w/3
                            ,wall*.5
                            ,(thickness_steps[i]<0?th-textheight_bridge:thickness_steps[i])])
                    linear_extrude(height=(thickness_steps[i]<0?textheight_bridge:textheight),convexity = 10)
                            text(str(abs(thickness_steps[i])),size=textsize_steps,font=font_bold,
                                halign="left",valign="baseline", spacing=0.85);
            }
            if (thickness_steps[i]<0) // bridge
            {
                gap=round/3;
                translate([ -one_steparea_w+gap,wall*stepareadistance,th-(-thickness_steps[i])-textheight_bridge ])
                    cube([one_steparea_w-gap, steparea_h, -thickness_steps[i]]);
            }
        }  
    }

    if(test_patterns=="enabled")
    {    
        rounded_square_test_pattern_raised(w,h,th,round,test_patterns);
    }

}
 

Debug="false";
if (Debug!="true")
{
    swatch(test_patterns);
}
else
{
    swatch("enabled");
    translate([0,h+3,0]) swatch("disabled");
}
